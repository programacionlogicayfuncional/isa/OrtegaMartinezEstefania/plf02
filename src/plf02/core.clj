(ns plf02.core)

(defn función-associative?-1
  [a]
  (associative? a))

(defn función-associative?-2
  [a]
  (associative? a))

(defn función-associative?-3
  [a]
  (associative? a))

(función-associative?-1 '(9 6 3 1 1/3 6/2))
(función-associative?-2 [9 5 1 5 9 3 8/8 1/9])
(función-associative?-3 {:a 10 :b 20 :c 30})

(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [a]
  (boolean? a))

(defn función-boolean?-3
  [a]
  (boolean? a))

(función-boolean?-1 "hola")
(función-boolean?-2 false)
(función-boolean?-3 8754)

(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [a]
  (char? a))

(defn función-char?-3
  [a]
  (char? a))

(función-char?-1 \a)
(función-char?-2 (first "Adiós"))
(función-char?-3 123)

(defn función-coll?-1
 [a]
 (coll? a))

(defn función-coll?-2
  [a]
  (coll? a))

(defn función-coll?-3
  [a]
  (coll? a))

(función-coll?-1 {:a 100 :b 200 :c 300})
(función-coll?-2 [])
(función-coll?-3 #{1 2 3 4})

(defn función-decimal?-1
  [a]
  (decimal? a))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 1M)
(función-decimal?-2 12345)
(función-decimal?-3 1/5)

(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))

(función-double?-1 234)
(función-double?-2 1.999)
(función-double?-3 1.0)

(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 1.9)
(función-float?-2 5/3)
(función-float?-3 0.000000)

(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

(función-ident?-1 :nombreCompleto)
(función-ident?-2 "adiós")
(función-ident?-3 975)

(defn función-indexed?-1
  [a]
  (indexed? a))

(defn función-indexed?-2
  [a]
  (indexed? a))

(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 [89 67 56 34])
(función-indexed?-2 ["hola" "hi" "hello"])
(función-indexed?-3 {:a 100 :b 200 :c 300})

(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a]
  (int? a))

(defn función-int?-3
  [a]
  (int? a))

(función-int?-1 "int")
(función-int?-2 nil)
(función-int?-3 -28/7)

(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer? a))

(defn función-integer?-3
  [a]
  (integer? a))

(función-integer?-1 986336)
(función-integer?-2 6/6)
(función-integer?-3 99.0)

(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))

(función-keyword?-1 :nombreMascota)
(función-keyword?-2 false)
(función-keyword?-3 :12345)

(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))

(función-list?-1 '(\a b \c 123 456 78 9 0))
(función-list?-2 (list "hola" "adiós" "hi"))
(función-list?-3 #{0 98 76 543 2 1})

(defn función-map-entry?-1
  [a]
  (map-entry? a))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

(función-map-entry?-1 (first {:a \a :b \b :c \c}))
(función-map-entry?-2 (second {:nombre "Estefania" :apellido "Ortega" :edad 23}))
(función-map-entry?-3 {:mascota "perro" :nombre "Fairy" :edad 4})

(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))

(función-map?-1 {})
(función-map?-2 {:nombre "Estefania" :apellido "Ortega" :edad 23})
(función-map?-3 (first {:mascota "perro" :nombre "Fairy" :edad 4}))

(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

(función-nat-int?-1 -999)
(función-nat-int?-2 6)
(función-nat-int?-3 -9/3)

(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 34567)
(función-number?-2 7/2)
(función-number?-3 "hola")

(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

(función-pos-int?-1 9/2)
(función-pos-int?-2 15/3)
(función-pos-int?-3 -7/7)

(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))

(función-ratio?-1 865/12)
(función-ratio?-2 9.999)
(función-ratio?-3 -543/12)

(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

(función-rational?-1 1.0)
(función-rational?-2 22/7)
(función-rational?-3 -30/3)

(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
  (seq? a))

(función-seq?-1 '(123 45 67 890))
(función-seq?-2 [000 987 654 321])
(función-seq?-3 (list "hola" "hi" "hello"))

(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 "Estefania")
(función-seqable?-2 12345)
(función-seqable?-3 [\a \b \c \d])

(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))

(función-sequential?-1 "adiós")
(función-sequential?-2 [0 89 67 345 21])
(función-sequential?-3 #{:a :b :c :d :e})

(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))

(defn función-set?-3
  [a]
  (set? a))

(función-set?-1 #{:a :b \a \b "hola"})
(función-set?-2 (hash-set "hola" "adios"))
(función-set?-3 #{})

(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 [])
(función-some?-2 nil)
(función-some?-3 #{1 2 3})

(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "")
(función-string?-2 "hola")
(función-string?-3 \a)

(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))

(función-symbol?-1 'Nombre)
(función-symbol?-2 \x)
(función-symbol?-3 '123)

(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 ["hola" "hi" "hello"])
(función-vector?-2 (vector :a :b :c :d :e))
(función-vector?-3 [])

(defn función-drop-1
  [a b]
  (drop a b))

(defn función-drop-2
  [a b]
  (drop a b))

(defn función-drop-3
  [a b]
  (drop a b))

(función-drop-1 3 [10 9 8 7 6 5 4 3 2 1])
(función-drop-2 2 '("hola" "hi" "hello"))
(función-drop-3 5 #{\a \b \c \d \e})

(defn función-drop-last-1
  [a]
  (drop-last a))

(defn función-drop-last-2
  [a b]
  (drop-last a b))

(defn función-drop-last-3
  [a b]
  (drop-last a b))

(función-drop-last-1 {:a 100 :b 200 :c 300 :d 400 :e 500})
(función-drop-last-2 2 {\a 1 \b 2 \c 3 \d 4 \e 5 \f 6})
(función-drop-last-3 0 [0 98 76 54 32 1])

(defn función-drop-while-1
  [a b]
  (drop-while a b))

(defn función-drop-while-2
  [a b]
  (drop-while a b))

(defn función-drop-while-3
  [a b]
  (drop-while a b))

(función-drop-while-1 neg? [-1 -2 -6 -7 1 2 3 4 0 1])
(función-drop-while-2 pos? '(10 20 30 -30 -20 -10))
(función-drop-while-3 boolean? [true false nil :abc 123 \P "hello"])

(defn función-every?-1
  [a b]
  (every? a b))

(defn función-every?-2
  [a b]
  (every? a b))

(defn función-every?-3
  [a b]
  (every? a b))

(función-every?-1 even? '(10 20 30 40 45))
(función-every?-2 nil? [nil nil nil nil nil])
(función-every?-3 #{90 85 80} [90 85 80 75])

(defn función-filterv-1
  [a b]
  (filterv a b))

(defn función-filterv-2
  [a b]
  (filterv a b))

(defn función-filterv-3
  [a b]
  (filterv a b))

(función-filterv-1 even? (range 50))
(función-filterv-2 string? '(\a "hi" \e "bye" \i "adiós" \o "hola"\u))
(función-filterv-3 char? '(\a "hi" \e "bye" \i "adiós" \o "hola" \u))

(defn función-group-by-1
  [a b]
  (group-by a b))

(defn función-group-by-2
  [a b]
  (group-by a b))

(defn función-group-by-3
  [a b]
  (group-by a b))

(función-group-by-1 char? '(\a "hi" \e "bye" \i "adiós" \o "hola" \u))
(función-group-by-2 count ["hi" "bye" "hola" "adios" "chao" "hello"])
(función-group-by-3 boolean? '(true true false false :a :b))

(defn función-iterate-1
  [a b]
  (iterate a b))

(defn función-iterate-2
  [a b]
  (iterate a b))

(defn función-iterate-3
  [a b]
  (iterate a b))

(función-iterate-1 inc 5)
(función-iterate-2 inc 20)
(función-iterate-3 inc 100)

(defn función-keep-1
  [a b]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))

(función-keep-1 {:a 1, :b 2, :c 3} [:a :b :c])
(función-keep-2 boolean? '(true true false false :a :b))
(función-keep-3 char? '(\a "hi" \e "bye" \i "adiós" \o "hola" \u))

(defn función-keep-indexed-1
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-3
  [a b]
  (keep-indexed a b))

(función-keep-indexed-1 vector "Hello")
(función-keep-indexed-2 list [:a :b :c])
(función-keep-indexed-3 hash-map "adiós")

(defn función-map-indexed-1
  [a b]
  (map-indexed a b))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [a b]
  (map-indexed a b))

(función-map-indexed-2 list #{"ay" "hay" "ahí"})
(función-map-indexed-1 vector "Morado")
(función-map-indexed-3 hash-set [:a :b :c :d :e])

(defn función-mapcat-1
  [a b]
  (mapcat a b))

(defn función-mapcat-2
  [a b c]
  (mapcat a b c))

(defn función-mapcat-3
  [a b c]
  (mapcat a b c))

(función-mapcat-1 concat [[:a :b :c] [:a :e :i :o :u]])
(función-mapcat-2 list [[1 2 3] [4 5 6] [7 8 9]] [1 2 3])
(función-mapcat-3 hash-map #{\a \e \i \o \u} '(:a :e :i :o :u))

(defn función-mapv-1
  [a b]
  (mapv a b))

(defn función-mapv-2
  [a b]
  (mapv a b))

(defn función-mapv-3
  [a b c d]
  (mapv a b c d))

(función-mapv-1 inc [9/9 8/4 7/2 6/2 5/3 1/2])
(función-mapv-2 dec [1 2 3 4 5 6 7 8 9 0])
(función-mapv-3 + [10 20 30] [40 50 60] [70 80 90])

(defn función-merge-with-1
  [a b c]
  (merge-with a b c))

(defn función-merge-with-2
  [a b c d]
  (merge-with a b c d))

(defn función-merge-with-3
  [a b c d e]
  (merge-with a b c d e))

(función-merge-with-1 - {:a 1  :b 2} {:a 9  :b 98 :c 10})
(función-merge-with-2 * {:a 100} {:a 22} {:a 32})
(función-merge-with-3 + {\a 22} {\a 33} {\a 44} {\a 55})

(defn función-not-any?-1
  [a b]
  (not-any? a b))

(defn función-not-any?-2
  [a b]
  (not-any? a b))

(defn función-not-any?-3
  [a b]
  (not-any? a b))

(función-not-any?-1 odd? '(22 44 66 88))
(función-not-any?-2 nil? [true false false true])
(función-not-any?-3 int? #{1/2 1/3 1/4 1/5})

(defn función-not-every?-1
  [a b]
  (not-every? a b))

(defn función-not-every?-2
  [a b]
  (not-every? a b))

(defn función-not-every?-3
  [a b]
  (not-every? a b))

(función-not-every?-1 odd? '(11 33 55 77 99))
(función-not-every?-2 nil? [true false false true])
(función-not-every?-3 int? #{1/2 1/3 1/4 1/5})

(defn función-partition-by-1
  [a b]
  (partition-by a b))

(defn función-partition-by-2
  [a b]
  (partition-by a b))

(defn función-partition-by-3
  [a b]
  (partition-by a b))

(función-partition-by-1 count ["a" "e" "i" "ou" "bc" "df" "ghj"])
(función-partition-by-2 odd? [1 1 1 2 2 3 3])
(función-partition-by-3 even? [2 2 2 2 9 9 9 9])

(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

(función-reduce-kv-1 assoc {} [1 2 3 4 5 6])
(función-reduce-kv-2 assoc {} ["uno" "dos" "tres"])
(función-reduce-kv-3 vector {:a 100 :b 200} [10 30 15])

(defn función-remove-1
  [a b]
  (remove a b))

(defn función-remove-2
  [a b]
  (remove a b))

(defn función-remove-3
  [a b]
  (remove a b))

(función-remove-1 pos? '(10 -10 -1/5 9.23))
(función-remove-2 boolean? [true "true" false "false"])
(función-remove-3 keyword? #{:nombre "Fayri" :edad 4})

(defn función-reverse-1
  [a]
  (reverse a))

(defn función-reverse-2
  [a]
  (reverse a))

(defn función-reverse-3
  [a]
  (reverse a))

(función-reverse-1 [:a :e :i :o :u])
(función-reverse-2 #{"hola" "hello" "hi" "bye" "adiós"})
(función-reverse-3 {:a 100 :e 200 :i 300 :o 400 :u 500})

(defn función-some-1
  [a b]
  (some a b))

(defn función-some-2
  [a b]
  (some a b))

(defn función-some-3
  [a b]
  (some a b))

(función-some-1 keyword? #{:nombre "Fayri" :edad 4})
(función-some-2 pos? '(10 -10 -1/5 9.23))
(función-some-3 string? [true "" false false 33])

(defn función-sort-by-1
  [a b]
  (sort-by a b))

(defn función-sort-by-2
  [a b]
  (sort-by a b))

(defn función-sort-by-3
  [a b]
  (sort-by a b))

(función-sort-by-1 count #{"hola" "hello" "hi" "bye" "adiós"})
(función-sort-by-2 string? [true "true" false "false" 33])
(función-sort-by-3 pos? '(0.01 56.10 -10 -1/5 9.23))

(defn función-split-with-1
  [a b]
  (split-with a b))

(defn función-split-with-2
  [a b]
  (split-with a b))

(defn función-split-with-3
  [a b]
  (split-with a b))

(función-split-with-1 string? ["false" "" "true" true false 33])
(función-split-with-2 int? [100 200 300 400 500 :a :e :i :o :u])
(función-split-with-3 odd? [1 3 5 6 7 9])

(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take a b))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 6 [9 75 235 7 223 0 764 224 876 43211 12])
(función-take-2 8 [555 444 333 222 111 666 777 888 999 000])
(función-take-3 2 #{"hola" "hi" "hello"})

(defn función-take-last-1
  [a b]
  (take-last a b))

(defn función-take-last-2
  [a b]
  (take-last a b))

(defn función-take-last-3
  [a b]
  (take-last a b))

(función-take-last-1 6 [9 75 235 7 223 0 764 224 876 43211 12])
(función-take-last-2 8 [555 444 333 222 111 666 777 888 999 000])
(función-take-last-3 2 #{"hola" "hi" "hello"})

(defn función-take-nth-1
  [a b]
  (take-nth a b))

(defn función-take-nth-2
  [a b]
  (take-nth a b))

(defn función-take-nth-3
  [a b]
  (take-nth a b))

(función-take-nth-1 20 (range 50))
(función-take-nth-2 4 [555 444 333 222 111 666 777 888 999 000])
(función-take-nth-3 3 #{9 75 235 7 223 0 764 224 876 43211 12})

(defn función-take-while-1
  [a b]
  (take-while a b))

(defn función-take-while-2
  [a b]
  (take-while a b))

(defn función-take-while-3
  [a b]
  (take-while a b))

(función-take-while-1 pos? [1/5 52 -57 90 654 1 0])
(función-take-while-2 keyword? [:a 100 :e 200 :i 300 :o 400 :u 500])
(función-take-while-3 neg? '(10 -10 -1/5 9.23))

(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [a b c d]
  (update a b c d))

(defn función-update-3
  [a b c]
  (update a b c))

(función-update-1 {:nombre "Estefania" :apellido "Ortega" :edad 23 } :edad dec)
(función-update-2 {:nombre "Berenice" :apellido "Martínez" :edad 33} :edad - 10)
(función-update-3 [100 22 48 964 235 9 75 0] 7 inc)

(defn función-update-in-1
  [a b c]
  (update-in a b c))

(defn función-update-in-2
  [a b c]
  (update-in a b c))

(defn función-update-in-3
  [a b c d]
  (update-in a b c d))

(función-update-in-1 [{:nombre "Estefania" :edad 23}  {:nombre "Berenice" :edad 30}] [1 :edad] dec)
(función-update-in-2 [{:mascota "perro" :edad 4 :nombre "Fairy"} {:mascota "gato" :nombre "c" :edad 5}] [1 :edad] inc)
(función-update-in-3 [{:a 100} {:b 200} {:c 300 :d 0}] [2 :d] + 400)
